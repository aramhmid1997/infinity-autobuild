#!/bin/bash
set -xT

# Override variables
if [ -n "$DEVELOP" ]; then
	export APP_VER="${DEV_APP_VER:-$APP_VER}"
	export IGNORE_BETA="${DEV_IGNORE_BETA:-$IGNORE_BETA}"
fi

export SRCDIR="$1"
export redirect_uri='http://127.0.0.1'
export APIUtils_file="app/src/main/java/ml/docilealligator/infinityforreddit/utils/APIUtils.java"
IS_BETA="$(echo "$APP_VER" | grep -i "beta")"
export IS_BETA

cd "$SRCDIR/Infinity-For-Reddit/" || {
	echo "Failed to cd to Infinity-For-Reddit"
	exit 1
}

# Check version
git checkout "${APP_VER}"

# Create artifact
last_ver() {
	echo "$APP_VER" >"$SRCDIR"/LAST_VER.txt
}

# Skip build if beta
if [[ -n "$IS_BETA" && "$IGNORE_BETA" == "yes" ]] && [ "$CI_PIPELINE_SOURCE" != "web" ]; then
	SKIP_BUILD=1
	echo "Skip Beta version"
fi

# Check new version
if [[ "$CI_PIPELINE_SOURCE" == "schedule" || "$CI_PIPELINE_SOURCE" == "push" ]] && [ "$LAST_VER" == "$APP_VER" ]; then
	SKIP_BUILD=1
	echo "No new version"
fi

if [ -n "$SKIP_BUILD" ]; then
	last_ver
	exit 0
fi

# Build Infinity
build_apk() {

	sed -i "s/NOe2iKrPPzwscA/$REDDIT_API/" "$APIUtils_file"
	sed -i "s|infinity://localhost|$redirect_uri|" "$APIUtils_file"
	sed -i "s|USER_AGENT = .*|USER_AGENT = \"android:personal-app:${APP_VER} (by /u/${USERNAME})\";|g" "$APIUtils_file"
	./gradlew :app:assembleMinifiedRelease
}

if build_apk; then
	echo "App built successfully"
else
	echo "Failed to build apk"
	exit 1
fi

# copy apk
APK="$SRCDIR/Infinity-For-Reddit/app/build/outputs/apk/minifiedRelease/app-minifiedRelease-unsigned.apk"
if [ -f "$APK" ]; then
	mkdir -p "$SRCDIR"/release
	cp "$APK" "$SRCDIR/release/Infinity-${APP_VER}-unsigned.apk" || {
		echo "Failed to copy apk"
		exit 1
	}

	# sign apk
	sign_apk() {
		"$SRCDIR"/scripts/apk-sign.sh "$SRCDIR/release/Infinity-${APP_VER}.apk" "$SRCDIR/release/Infinity-${APP_VER}-unsigned.apk"
	}
	if sign_apk; then
		echo "Apk signed successfully"
	else
		echo "Failed to sign apk"
		exit 1
	fi

fi

# upload apk to gitlab
APK_SIGNED="$SRCDIR/release/Infinity-${APP_VER}.apk"
UPLOAD_URL="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/Infinity/${APP_VER}/Infinity-${APP_VER}.apk"
if [ -f "$APK_SIGNED" ]; then
	upload_artifact() {
		curl -s --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file "$APK_SIGNED" "$UPLOAD_URL"
	}
	if upload_artifact; then
		echo "Uploaded apk"
		export SIGN_STATUS="OK"
	else
		echo "Failed to upload apk"
		exit 1
	fi
fi

# create artifact
if [ "$SIGN_STATUS" = "OK" ]; then
	last_ver
fi

# Send notifications

URL="https://api.github.com/repos/Docile-Alligator/Infinity-For-Reddit/releases/tags/$APP_VER"
CHANGELOG="$(curl -s -H "Accept: application/vnd.github+json" "$URL" | awk -F'"' '/"body":/ {print $4}')"

read -r -d '' NTFY_MESSAGE <<EOM
Infinity ${APP_VER} released.
Changelog:
$(echo -e $CHANGELOG)
EOM

# ntfy.sh
if [ -n "$NTFY_TOPIC" ]; then
	"$SRCDIR/scripts/ntfy.sh" "$NTFY_MESSAGE" "$UPLOAD_URL"
fi

# Telegram
if [ -n "$TG_TOKEN" ] && [ -n "$TG_CHATID" ]; then
	"$SRCDIR/scripts/telegram.sh" "$NTFY_MESSAGE" "$APK_SIGNED"
fi
