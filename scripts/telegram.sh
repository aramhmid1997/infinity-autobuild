#!/usr/bin/env bash

set -x

BOTTOKEN="$TG_TOKEN"
CHATID="$TG_CHATID"
MESSAGE="$1"
APK="$2"

curl -s \
	-F chat_id="$CHATID" \
	-F document=@"$APK" \
	-F caption="$MESSAGE" \
	-F parse_mode="Markdown" \
	"https://api.telegram.org/bot${BOTTOKEN}/sendDocument"
